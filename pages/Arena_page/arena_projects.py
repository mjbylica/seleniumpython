from selenium.webdriver.common.by import By
import time
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaProjects:
    def __init__(self, browser):
        self.browser = browser

    def create_new_project(self, nazwa_projektu, prefix):
        self.browser.find_element(By.CSS_SELECTOR, 'a[href$=add_project]').click()
        self.nazwa_projektu = nazwa_projektu
        self.browser.find_element(By.ID, 'name').send_keys(str(nazwa_projektu) + "Marta_UI")
        self.browser.find_element(By.ID, 'prefix').send_keys(prefix)
        self.browser.find_element(By.ID, 'description').send_keys("projekt testowy")
        self.browser.find_element(By.ID, 'save').click()
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_label_title').text == (nazwa_projektu) + "Marta_UI"
        self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
        self.browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(str(nazwa_projektu) + "Marta_UI")
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()
        assert self.browser.find_element(By.CSS_SELECTOR, '.t_number').text == prefix
