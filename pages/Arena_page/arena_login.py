from selenium.webdriver.common.by import By


class ArenaLoginPage:

    def __init__(self, browser):
        self.browser = browser

    def login_page(self, email, password):
        self.browser.find_element(By.ID, 'email').send_keys(email)
        self.browser.find_element(By.ID, 'password').send_keys(password)
        self.browser.find_element(By.ID, 'login').click()