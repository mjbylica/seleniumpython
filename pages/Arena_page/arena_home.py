from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait



class ArenaHomePage:
    def __init__(self, browser):
        self.browser = browser

    def verify_user_email(self, user_email):
        user_info = self.browser.find_element(By.CSS_SELECTOR, '.user-info small')
        assert user_info.text == user_email

    def select_admin_panel_icon(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
        wait = WebDriverWait(self.browser, 10)
        menu_name = self.browser.find_element(By.CSS_SELECTOR, '.activeMenu')
        wait.until(expected_conditions.element_to_be_clickable(menu_name))
