import pytest
import lorem
from utils.random_message import generate_random_text
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

from pages.Arena_page.arena_login import ArenaLoginPage
from pages.Arena_page.arena_home import ArenaHomePage
from pages.Arena_page.arena_projects import ArenaProjects

user_email = 'administrator@testarena.pl'
user_password = 'sumXQQ72$L'
nazwa_projektu = lorem.sentence()
prefix = generate_random_text(6)


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login = ArenaLoginPage(driver)
    arena_login.login_page(user_email, user_password)
    yield driver
    driver.quit()


def test_login_to_testArena(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.verify_user_email(user_email)


def test_should_open_Admin_Panel(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.select_admin_panel_icon()


def test_should_create_new_project(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.select_admin_panel_icon()
    arena_projects = ArenaProjects(browser)
    arena_projects.create_new_project(nazwa_projektu, prefix)

